var mysql = require('mysql');

// dont push this...
var pool  = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'NOPE',
  database: 'NOPE'
});

pool.on('enqueue', function() {
  console.log('Waiting for available connection slot');
});

pool.on('connection', function(connection) {
  console.log('MySQL connection made');
});

module.exports = {
  getConnection: function(callback) {
    pool.getConnection(function(err, connection) {
      callback(err, connection);
    });
  },

  query: function(query, callback) {
    pool.getConnection(function(err, connection) {
      if (err == null) {
        connection.query(query, function(err, result, fields) {
          if (err == null) {
            callback(null, result, fields);
          } else {
            console.log('\x1b[33mMySQL query error\x1b[0m');
            console.log(err);
            callback(err);
          }
          connection.release();
        });
      }
      else {
        console.log('\x1b[33mPool connection error\x1b[0m');
        console.log(err);
        callback('Pool connection error');
      }
    });
  }
}
