var express = require('express');
var fs = require('fs');
var db = require('../utils/mysql');
var router = express.Router();

String.prototype.format = function() {
  var formatted = this;
  for (var i = 0; i < arguments.length; i++) {
    var regexp = new RegExp('\\{'+i+'\\}', 'gi');
    formatted = formatted.replace(regexp, arguments[i]);
  }

  return formatted;
};

function addVisit(ip) {
  var d = new Date();
  d = d.toISOString().substring(0, 19).replace('T', ' ');
  var query = 'INSERT INTO visits (location, visitor, date) VALUES ("{0}", "{1}", "{2}")';
  console.log(query.format('hallb.org', ip, d));
  db.query(query.format('hallb.org', ip, d), function(err, result, fields) {
    if (err != null) {
      console.log('sql insert err :' + err);
    }
  });
}

function addUniqueIp(ip) {
  var query = 'INSERT INTO unique_ips (ipv4) VALUES ("{0}")';
  db.query(query.format(ip), function(err, result, fiedls) {
    if (err != null) {
      console.log('sql insert err :' + err);
    }
  });
}

function checkIp(ip) {
  var query = 'SELECT ipv4 FROM unique_ips WHERE ipv4 = "{0}"';
  db.query(query.format(ip), function(err, result, fields) {
    if (err == null) {
      if (result.length > 0) {
        console.log('old ip');
        // ip exists in the database
        // just add it to visits
        addVisit(ip);
      }
      else {
        console.log('new ip');
        // new ip! add it to unique_ips
        // and add it to visits
        addUniqueIp(ip);
        addVisit(ip);
      }
    }
    else {
      console.log('error' + err);
    }
  });
}

/* GET home page. */
router.get('/', function(req, res, next) {
  var ip = req.headers['x-real-ip'];
  if (ip == undefined) ip = '127.0.0.1';
  checkIp(ip);
  res.render('index', {
    title: 'Start',
    selected: 0
  });
});

router.get('/utbildning', function(req, res, next) {
  res.render('edu', {
    title: 'Utbildning',
    selected: 1
  });
});

router.get('/portfolio', function(req, res, next) {
  res.render('portfolio', {
    title: 'Portfolio',
    selected: 2
  });
});

router.get('/server', function(req, res, next) {
  res.render('server', {
    title: 'Denna server',
    selected: 3
  });
});
/* Displays a pdf */
router.get('/pdf', function(req, res, netx) {
  var pathToPdf = '/bitbucket-projects/hallb.org/public/cert.pdf';
  fs.readFile(pathToPdf, function(err, data) {
    res.contentType('application/pdf');
    res.end(data);
  });
});
module.exports = router;
