# README #

### What is hallb.org? ###
www.hallb.org points to my own server in my home. 
To be able to ssh the server you need to be connected to the local network as the router only forwards http requests (port 80).

www.hallb.org was chosen as domain name due to the convenient name (my last name is hallborg) 

This server is currently running two services. A “Who is Jesper Hallborg” - which can be found at www.hallb.org

The second service is a web site for hallborgs häsla, which is a company that focus on alternative medicine, mainly via zone therapy. It’s located via www.halsa.hallb.org

Structure of the server and flows in the system

![asdf.png](https://bitbucket.org/repo/eMpA8x/images/3470351012-asdf.png)